from django.shortcuts import render, redirect
from .forms import NewsCommentary
from .models import Commentary

# Create your views here.
def newsPage(request):
    commentaries = Commentary.objects.all()
    form = NewsCommentary()
    if (request.method == 'POST') :
        form = NewsCommentary(request.POST or None)
        if (form.is_valid):
            form.save()
            return redirect('news:newsPage')
    
    context = {'news_commentary' : form, 'commentaries' : commentaries}
    return render(request, 'news/news.html', context)