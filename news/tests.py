from django.test import TestCase, Client
from http import HTTPStatus
from django.urls import resolve, reverse
from .forms import NewsCommentary
from .models import Commentary
from . import views

# Create your tests here.
class TestNews(TestCase) :
    def test_url_news_exist(self) :
        response = Client().get('/news/')
        self.assertEquals(response.status_code, 200)

    def test_news_template(self) :
        response = Client().get('/news/')
        self.assertTemplateUsed(response, 'news/news.html')

    def test_news_title(self) :
        response = Client().get('/news/')
        html_news = response.content.decode('utf8')
        self.assertIn("News About", html_news)
        self.assertIn("Covid-19", html_news)

    def test_lastest_news(self) :
        response = Client().get('/news/')
        html_news = response.content.decode('utf8')
        self.assertIn("Lastest News", html_news)

    def test_commentary_model_exist(self) :
        Commentary.objects.create(name="Name", comment="Comment")
        count_comment = Commentary.objects.all().count()
        self.assertEquals(count_comment, 1)

    def test_newsPage_is_used(self) :
        function = resolve('/news/')
        self.assertEqual(function.func, views.newsPage)

    def test_comment_success(self) :
        response = self.client.post('/news/', data={'name': 'THIS IS NAME', 'comment': 'THIS IS COMMENT'})
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
    

