from django import forms

from .models import Commentary

class NewsCommentary(forms.ModelForm):
    class Meta :
        model = Commentary
        fields = [
            'name', 'comment',
        ]

        widgets = {
            'name' : forms.TextInput(attrs={'class': 'form-control'}),
            'comment' : forms.Textarea(attrs={'class': 'form-control'}),
        }