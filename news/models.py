from django.db import models
from datetime import datetime

# Create your models here.
class Commentary(models.Model):
    name = models.CharField(max_length=30, default="")
    comment = models.TextField()
    date = models.DateTimeField(default=datetime.now, blank=True)

