from django.shortcuts import render
from .models import status

# Create your views here.
def infografis(request):
    statusharian = status.objects.all()
    re = {'status':statusharian}
    return render(request, 'main/infografis.html', re)