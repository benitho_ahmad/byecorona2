from django.apps import AppConfig


class PreventionsConfig(AppConfig):
    name = 'preventions'
