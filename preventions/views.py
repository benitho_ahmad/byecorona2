from django.shortcuts import render, redirect
from .models import Message
from .forms import MessageForm

def preventions(request):
    messagesList = Message.objects.all()
    form = MessageForm(request.POST or None)
    if (form.is_valid()):
        form.save()
        return redirect('/preventions/#share-tips')
    context = {
        'form' : form,
        'list' : messagesList
    }
    return render(request, 'preventions/preventions.html', context)
