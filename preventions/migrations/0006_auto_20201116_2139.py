# Generated by Django 3.1.3 on 2020-11-16 14:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('preventions', '0005_auto_20201116_1736'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='message',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='message',
            name='name',
            field=models.CharField(blank=True, max_length=15),
        ),
    ]
