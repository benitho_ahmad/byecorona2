from django.forms import ModelForm
from django import forms
from .models import Message

class MessageForm(forms.ModelForm):
	class Meta:
		model = Message
		fields = (
            'name',
		    'message',
		)

		widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter Your Name'}),
			'message': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Enter Your Message'}),
		}