from django.test import TestCase, Client
from django.urls import resolve, reverse
from http import HTTPStatus
from .models import Message
from .forms import MessageForm
from . import views

# Create your tests here.
class TestPreventions(TestCase):
    def test_model_used_in_preventions_html(self):
        Message.objects.create(name="THIS IS NAME", message="THIS IS MESSAGE")
        count_the_message = Message.objects.all().count()
        self.assertEquals(count_the_message, 1)
    
    def test_str_equal_to_name(self):
        Message.objects.create(name="THIS IS NAME", message="THIS IS MESSAGE")
        obj = Message.objects.get(pk=1)
        self.assertEqual(str(obj), obj.name)

    def test_function_views_preventions_used(self):
        function = resolve('/preventions/')
        self.assertEqual(function.func, views.preventions)
        
    def test_post_success(self):
        response = self.client.post('/preventions/', data={'name': 'THIS IS NAME', 'message': 'THIS IS MESSAGE'})
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(Message.objects.count(), 1)