from django.shortcuts import render, redirect
from .models import Feedback
from .forms import FeedbackForm

def feedback(request):
    inputan = Feedback.objects.all()
    form = FeedbackForm(request.POST or None)
    if (form.is_valid()):
        form.save()
        return redirect('/peta/#feedback')
    context = {'form' : form, 'inputan':inputan}
    return render(request, 'peta/peta.html', context)
