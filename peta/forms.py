from django.forms import ModelForm
from django import forms
from .models import Feedback

class FeedbackForm(forms.ModelForm):
	class Meta:
		model = Feedback
		fields = (
            'name',
		    'feedback',
		)

		widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
			'feedback': forms.Textarea(attrs={'class': 'form-control'}),
		}