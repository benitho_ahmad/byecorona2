from django.test import TestCase, Client
from http import HTTPStatus
from django.urls import resolve, reverse
from .models import Feedback
from . import views
from .forms import FeedbackForm

# Create your tests here.
class TestFeedback(TestCase):
    def test_model_peta(self):
        Feedback.objects.create(name="THIS IS NAME", feedback="THIS IS MESSAGE")
        hitung = Feedback.objects.all().count()
        self.assertEquals(hitung, 1)
    
    def test_cek_nama(self):
        Feedback.objects.create(name="THIS IS NAME", feedback="THIS IS MESSAGE")
        obj = Feedback.objects.get(pk=1)
        self.assertEqual(str(obj), obj.name)

    def test_views_peta(self):
        function = resolve('/peta/')
        self.assertEqual(function.func, views.feedback)
        
    def test_post_success(self):
        response = self.client.post('/peta/', data={'name': 'THIS IS NAME', 'feedback': 'THIS IS MESSAGE'})
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
