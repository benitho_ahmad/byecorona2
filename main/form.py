from django import forms
from .models import pengunjung


class InputForm(forms.ModelForm):
    class Meta:
        model = pengunjung
        fields = ["Visitor_Name", "Email"]

    input_attrs_nama = {
		'placeholder' : 'Visitor Name'
	}
    Visitor_Name = forms.CharField(max_length=50, widget=forms.TextInput(attrs=input_attrs_nama))

    input_attrs_email = {
		'placeholder' : 'Email'
	}
    Email = forms.EmailField(max_length = 254, widget=forms.TextInput(attrs=input_attrs_email))