from django.shortcuts import render
from django.http import HttpResponseRedirect
from .form import InputForm
from .models import pengunjung

def home(request):
    jumlh = pengunjung.objects.count()
    context = {'form' : InputForm, 'jumlah' : jumlh}
    return render(request, 'main/home.html', context)

def savedata(request):
    form = InputForm(request.GET or None)
    if (form.is_valid() and request.method == "GET"):
        form.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')
